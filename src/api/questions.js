import { QUESTIONS_URL } from ".";


export async function apiFetchQuestions() {
    try{
        const response = await fetch (`${QUESTIONS_URL}`)

        if(!response.ok) {
            throw new Error("Could not find questions")
        }

        //Destructing
        const {success, data, error = "Could not fetch questions"} = await response.json()

        if(!success) {
            throw new Error(error)
        }
        //results=questions
        return [null, data]
    }
    catch (e) {
        return [e.message, []]
    }
}

