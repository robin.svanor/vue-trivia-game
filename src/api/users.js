import { API_KEY, BASE_URL } from "./";


// Function to register user
export async function apiCheckUser( username ) {
    try {
        const config = {
            method: "POST",
            headers: {
                "X-API-Key": API_KEY,
                "content-type": "application/json"
            },
            body: JSON.stringify({
                username,
                highScore: 0,
                score: 0
            })
        }
        // The BASE_URL comes from /api/index
        const response = await fetch( `${BASE_URL}`, config )
        const {success, data, error = "An error occurred while registering user"} = await response.json()
        if (!success) {
            throw new Error(error)
        }

        // Null is potential error, data is the user
        return [null, data]
    } catch(error) {
        return [error.message, null]
    }
}


// Function for getting user
export async function apiGetUser( username ) {
    try {
        const response = await fetch( `${BASE_URL}?username=${username}`)
        const { success, data, error = "An error occurred while getting user" } = await response.json()

        if ( !success ) {
            throw new Error( error )
        }

        
        return [ null, data ]
    } catch( error ) {
        return [ error.message, null ]
    }
}


// Function for updating a users score
export async function apiUpdateUserScore( username, highScore ) {
    try {
        const config = {
            method: "PATCH",
            headers: {
                "X-API-Key": API_KEY,
                "content-type": "application/json"
            },
            body: JSON.stringify({
                    highScore
            })
        }
        
        const response = await fetch( `${BASE_URL}?username=${username}`, config )
        const { success, data, error = "An error occurred while updating highscore" } = await response.json()
        if ( !success ) {
            throw new Error( error )
        }

        return [ null, data ]
    } catch( error ) {
        return [ error.message, null ]
    }
}