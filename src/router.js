import { createRouter, createWebHistory } from "vue-router";
import Questions from "./views/Questions.vue";
import StartPage from "./views/StartPage.vue";
import Result from "./views/Result.vue";


const routes = [
    {
        path: "/",
        component: StartPage
    },
    {
        path: "/questions",
        component: Questions
    },
    {
        path: "/result",
        component: Result
    }
]

// Creates web history 
export default createRouter({
    history: createWebHistory(),
    routes
})