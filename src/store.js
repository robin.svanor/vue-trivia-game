import { createStore } from "vuex";
import { apiFetchQuestions } from "./api/questions";
import { apiCheckUser } from "./api/users";


// function for initializing user
const initUsername = () => {
    const storedUser = localStorage.getItem("trivia-user")
    if ( !storedUser ) {
        return null
    }

    return JSON.parse(storedUser)
}


export default createStore({
    state: {
        user: initUsername(),
        questions: []
    },

    mutations: {
        setUsername: ( state, user ) => {
            state.user = user
        },
        setQuestions: (state, questions) => {
            state.questions = questions
        }
    },

    actions: {
        async saveUsername({ commit }, { username, action }) {
            try {

                if ( action !== "getUser" ) {
                    throw new Error("GetUser: Unknown action provided " + action)
                }

                const [error, user] = await apiCheckUser(username.value, action)

                if ( error !== null ) {
                    throw new Error(error)
                }

                commit("setUsername", user)
                localStorage.setItem( "trivia-user", JSON.stringify(user))

                return null

            } catch (error) {
                return error.message
            }
        },
        async fetchQuestions({commit}) {
            const [error, questions] = await apiFetchQuestions()
            if (error !== null) {
                return error
            }

            commit("setQuestions", questions)
            return null
        }
    }
})